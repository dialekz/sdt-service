package com.sdt.api

import akka.http.scaladsl.model.{ContentTypes, StatusCodes}
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.testkit.ScalatestRouteTest
import com.sdt.BaseSampleData
import com.sdt.exception.{BaseExceptionHandler, EventNotFoundException}
import com.sdt.model._
import com.sdt.service.EventService
import com.typesafe.scalalogging.LazyLogging
import org.scalamock.scalatest.MockFactory
import org.scalatest.funspec.AnyFunSpec

import java.time.format.DateTimeFormatter
import scala.concurrent.Future

class UserEventApiTest extends AnyFunSpec
  with MockFactory
  with ScalatestRouteTest
  with ApiJsonSupport
  with LazyLogging
  with BaseSampleData {

  private val mockEventService: EventService = mock[EventService]

  val route: Route = Route.seal(
    new UserEventApi(mockEventService).route
  )(exceptionHandler = BaseExceptionHandler.exceptionHandler)

  describe("GET http://localhost:8080/event/{eventId}") {
    it("return event state by eventId") {
      mockEventService.getEventById _ expects BASE_EVENT_ID returns Future.successful(sampleEvent)

      Get("/event/1") ~> route ~> check {
        logger.debug(s"Response:\n${responseAs[String]}")
        assert(status == StatusCodes.OK)
        assert(contentType == ContentTypes.`application/json`)
        assert(responseAs[Option[Event]].contains(sampleEvent))
      }
    }

    it("return error if event is not found") {
      mockEventService.getEventById _ expects BASE_EVENT_ID returns Future.failed(EventNotFoundException(BASE_EVENT_ID))

      Get("/event/1") ~> route ~> check {
        logger.debug(s"Response:\n${responseAs[String]}")
        assert(status == StatusCodes.BadRequest)
        assert(contentType == ContentTypes.`application/json`)
        assert(responseAs[String].contains(makeErrorJsonForEventId(1)))
      }
    }
  }

  describe("PUT http://localhost:8080/event/create") {
    it("create event from request body") {
      val eventWithoutId = sampleEvent.copy(id = None)
      mockEventService.createEvent _ expects eventWithoutId returns Future.successful(sampleEvent)

      Put("/event/create").withEntity(ContentTypes.`application/json`, makeJsonForEvent(eventWithoutId)) ~> route ~> check {
        logger.debug(s"Response:\n${responseAs[String]}")
        assert(status == StatusCodes.OK)
        assert(contentType == ContentTypes.`application/json`)
        assert(responseAs[Option[Event]].contains(sampleEvent))
      }
    }
  }

  describe("POST http://localhost:8080/event/{eventId}/update") {
    it("update event for path parameter eventId with request json body") {
      mockEventService.updateEvent _ expects(BASE_EVENT_ID, sampleEvent) returns Future.successful(sampleEvent)

      Post("/event/1/update").withEntity(ContentTypes.`application/json`, makeJsonForEvent(sampleEvent)) ~> route ~> check {
        logger.debug(s"Response:\n${responseAs[String]}")
        assert(status == StatusCodes.OK)
        assert(contentType == ContentTypes.`application/json`)
        assert(responseAs[Option[Event]].contains(sampleEvent))
      }
    }
  }

  describe("POST http://localhost:8080/event/{eventId}/start") {
    it("update event with started status") {
      mockEventService.startEvent _ expects BASE_EVENT_ID returns Future.successful(sampleEvent)

      Post("/event/1/start") ~> route ~> check {
        logger.debug(s"Response:\n${responseAs[String]}")
        assert(status == StatusCodes.OK)
        assert(responseAs[Option[Event]].contains(sampleEvent))
      }
    }
  }

  describe("POST http://localhost:8080/event/{eventId}/finish") {
    it("update event with finished status") {
      mockEventService.finishEvent _ expects BASE_EVENT_ID returns Future.successful(sampleEvent)

      Post("/event/1/finish") ~> route ~> check {
        logger.debug(s"Response:\n${responseAs[String]}")
        assert(status == StatusCodes.OK)
        assert(responseAs[Option[Event]].contains(sampleEvent))
      }
    }
  }

  describe("POST http://localhost:8080/event/{eventId}/reject") {
    it("update event with rejected status") {
      mockEventService.rejectEvent _ expects BASE_EVENT_ID returns Future.successful(sampleEvent)

      Post("/event/1/reject") ~> route ~> check {
        logger.debug(s"Response:\n${responseAs[String]}")
        assert(status == StatusCodes.OK)
        assert(responseAs[Option[Event]].contains(sampleEvent))
      }
    }
  }

  private def makeJsonForEvent(event: Event): String = {
    val result = new StringBuilder("{")

    if (event.id.nonEmpty) result ++= s"""\"id\": ${event.id.get},"""

    result ++=
      s"""\"eventType\": \"${event.eventType.name}\",""" +
        s"""\"status\": ${event.status.code},""" +
        s"""\"plannedStartTime\": \"${event.plannedStartTime.format(DateTimeFormatter.ISO_DATE_TIME)}\",""" +
        s"""\"plannedFinishTime\": \"${event.plannedFinishTime.format(DateTimeFormatter.ISO_DATE_TIME)}\"""" +
        "}"
    result.result()
  }

  private def makeErrorJsonForEventId(eventId: Int) = s"""{\"errorMessage\":\"Event with id=$eventId is not found\"}"""
}
