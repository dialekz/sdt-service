package com.sdt

import com.sdt.model.{Event, EventStatus, EventType}

import java.time.ZonedDateTime

trait BaseSampleData {

  val BASE_EVENT_ID: Long = 1

  lazy val sampleEvent = Event(
    id = Some(BASE_EVENT_ID),
    eventType = EventType.PlannedWork,
    status = EventStatus.EventCreated,
    plannedStartTime = ZonedDateTime.now.plusSeconds(1),
    plannedFinishTime = ZonedDateTime.now.plusSeconds(10)
  )

}
