package com.sdt.service.impl

import com.sdt.BaseSampleData
import com.sdt.exception.EventNotFoundException
import com.sdt.model.{Event, EventStatus}
import com.sdt.repository.EventRepository
import com.sdt.service.EventStatusSchedulerService
import org.scalamock.scalatest.MockFactory
import org.scalatest.{BeforeAndAfter, Ignore}
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

@Ignore
class EventServiceImplTest extends AnyFunSuite with MockFactory with Matchers with BaseSampleData with BeforeAndAfter {

  private var mockEventRepository: EventRepository = _
  private var mockEventStatusSchedulerService: EventStatusSchedulerService = _

  private var eventService: EventServiceImpl = _

  before {
    mockEventRepository = mock[EventRepository]
    mockEventStatusSchedulerService = mock[EventStatusSchedulerService]
    eventService = new EventServiceImpl(mockEventRepository, mockEventStatusSchedulerService)
  }

  test("getEventById should return event") {
    mockEventRepository.getById _ expects BASE_EVENT_ID returns Future.successful(Some(sampleEvent))

    for {
      actual <- eventService.getEventById(BASE_EVENT_ID)
      _ = Thread.sleep(5000) // TODO should find some other way
    } yield assert(actual == sampleEvent)
  }

  test("getEventById should return EventNotFoundException") {
    mockEventRepository.getById _ expects BASE_EVENT_ID returns Future.successful(None)

    for {
      _ <- eventService.getEventById(BASE_EVENT_ID)
      _ = Thread.sleep(5000) // TODO should find some other way
    } yield assertThrows(EventNotFoundException(BASE_EVENT_ID))
  }

  test("createEvent should return created event and call createTaskToStartEvent") {
    val eventWithoutId = sampleEvent.copy(id = None)
    mockEventRepository.add _ expects eventWithoutId returns Future.successful(sampleEvent)

    for {
      actual <- eventService.createEvent(eventWithoutId)
      _ = Thread.sleep(5000) // TODO should find some other way
    } yield (
      assert(actual == sampleEvent),
      (mockEventStatusSchedulerService.createTaskToStartEvent _).expects(sampleEvent).once()
    )
  }

  test("update should return updated event") {
    val updatedEvent: Event = sampleEvent.copy(status = EventStatus.EventInProgress)
    mockEventRepository.updateById _ expects(BASE_EVENT_ID, updatedEvent) returns Future.successful(1)
    mockEventRepository.getById _ expects BASE_EVENT_ID returns Future.successful(Some(updatedEvent))

    for {
      actual <- eventService.updateEvent(BASE_EVENT_ID, updatedEvent)
      _ = Thread.sleep(5000) // TODO should find some other way
    } yield (
      assert(actual == updatedEvent),
      (mockEventStatusSchedulerService.createTaskToFinishEvent _).expects(updatedEvent).once()
    )
  }

  test("startEvent should return started event and call createTaskToFinishEvent") {
    val expectedEvent: Event = sampleEvent.copy(status = EventStatus.EventInProgress)
    mockEventRepository.updateStatusById _ expects(BASE_EVENT_ID, EventStatus.EventInProgress) returns Future.successful(1)
    mockEventRepository.getById _ expects BASE_EVENT_ID returns Future.successful(Some(expectedEvent))

    for {
      actual <- eventService.startEvent(BASE_EVENT_ID)
      _ = Thread.sleep(5000) // TODO should find some other way
    } yield (
      assert(actual == expectedEvent),
      (mockEventStatusSchedulerService.createTaskToFinishEvent _).expects(expectedEvent).once()
    )
  }

  test("finishEvent should return updated event with status EventFinished") {
    val expectedEvent: Event = sampleEvent.copy(status = EventStatus.EventFinished)
    mockEventRepository.updateStatusById _ expects(BASE_EVENT_ID, EventStatus.EventFinished) returns Future.successful(1)
    mockEventRepository.getById _ expects BASE_EVENT_ID returns Future.successful(Some(expectedEvent))

    for {
      actual <- eventService.finishEvent(BASE_EVENT_ID)
      _ = Thread.sleep(5000) // TODO should find some other way
    } yield assert(actual == expectedEvent)
  }

  test("rejectEvent should return updated event with status EventRejected") {
    val expectedEvent: Event = sampleEvent.copy(status = EventStatus.EventRejected)
    mockEventRepository.updateStatusById _ expects(BASE_EVENT_ID, EventStatus.EventRejected) returns Future.successful(1)
    mockEventRepository.getById _ expects BASE_EVENT_ID returns Future.successful(Some(expectedEvent))

    for {
      actual <- eventService.rejectEvent(BASE_EVENT_ID)
      _ = Thread.sleep(5000) // TODO should find some other way
    } yield assert(actual == expectedEvent)
  }

}
