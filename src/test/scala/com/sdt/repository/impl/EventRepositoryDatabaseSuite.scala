package com.sdt.repository.impl

import com.sdt.BaseSampleData
import org.scalactic.source
import org.scalatest.BeforeAndAfter
import org.scalatest.compatible.Assertion
import org.scalatest.funsuite.AsyncFunSuite
import slick.dbio.{Effect, NoStream}
import slick.jdbc.JdbcBackend.Database
import slick.jdbc.H2Profile.api._
import slick.jdbc.JdbcBackend

import java.util.UUID
import scala.concurrent.Future

class EventRepositoryDatabaseSuite extends AsyncFunSuite with BaseSampleData with BeforeAndAfter {

  protected val SampleEvents = Seq(sampleEvent)

  protected var testH2db: JdbcBackend.Database = _
  protected var eventRepositoryDatabase: EventRepositoryDatabase = _

  before {
    testH2db = Database.forURL(
      url = s"jdbc:h2:mem:${UUID.randomUUID}",
      driver = "org.h2.Driver",
      keepAliveConnection = true
    )
    eventRepositoryDatabase = new EventRepositoryDatabase(testH2db)
  }

  protected def test[R, S <: NoStream, E <: Effect](testName: String)
                                                   (testFun: => Future[Assertion])
                                                   (implicit pos: source.Position): Unit = {
    super.test(testName) {
      testH2db.run(
        eventRepositoryDatabase.allEvents.schema.create
          .andThen(eventRepositoryDatabase.allEvents ++= SampleEvents)
      )
        .flatMap(_ => testFun)
        .andThen { case _ => testH2db.close() }
    }
  }

}
