package com.sdt.repository.impl

import com.sdt.BaseSampleData
import com.sdt.model._
import org.scalatest.matchers.should.Matchers

class EventRepositoryTest extends EventRepositoryDatabaseSuite with Matchers with BaseSampleData {

  test("getById should return right event") {
      for {
        actual <- eventRepositoryDatabase.getById(BASE_EVENT_ID)
      } yield assert(actual.contains(SampleEvents.head))
  }

  test("add should return event with auto incremented id") {
      for {
        event <- eventRepositoryDatabase.add(sampleEvent)
        actual <- eventRepositoryDatabase.getById(event.id.get)
      } yield assert(actual.contains(sampleEvent.copy(id = Some(SampleEvents.size + 1))))
  }

  test("updateById should update event in database") {
      val expected = SampleEvents.head.copy(eventType = EventType.ServiceCrash, status = EventStatus.EventInProgress)
      for {
        rows <- eventRepositoryDatabase.updateById(SampleEvents.head.id.get, expected)
        actual <- eventRepositoryDatabase.getById(SampleEvents.head.id.get)
      } yield {
        assert(rows == 1)
        assert(actual.contains(expected))
      }
  }

  test("updateStatusById should update status in database") {
      val expected = SampleEvents.head.copy(status = EventStatus.EventRejected)
      for {
        rows <- eventRepositoryDatabase.updateStatusById(SampleEvents.head.id.get, EventStatus.EventRejected)
        actual <- eventRepositoryDatabase.getById(SampleEvents.head.id.get)
      } yield {
        assert(rows == 1)
        assert(actual.contains(expected))
      }
  }

}
