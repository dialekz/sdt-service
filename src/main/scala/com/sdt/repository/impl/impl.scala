package com.sdt.repository

import com.sdt.model.EventStatus.EventStatus
import com.sdt.model.EventType.EventType
import com.sdt.model._
import slick.jdbc
import slick.jdbc.H2Profile.MappedColumnType
import slick.jdbc.H2Profile.api._
import slick.jdbc.JdbcBackend
import slick.jdbc.JdbcBackend.Database

import java.util.UUID

package object impl {

  val dbH2Local: JdbcBackend.Database = Database.forURL(
    url = s"jdbc:h2:mem:${UUID.randomUUID}",
    driver = "org.h2.Driver",
    keepAliveConnection = true
  )

  val postgresDB: JdbcBackend.Database = Database.forConfig("postgresDB")

  implicit val eventTypeMapper = MappedColumnType.base[EventType, Int](
    eventType => eventType.id,
    typeId => EventType.byId(typeId)
  )

  implicit val eventStatusMapper = MappedColumnType.base[EventStatus, Int](
    eventStatus => eventStatus.id,
    statusId => EventStatus.byId(statusId)
  )
}
