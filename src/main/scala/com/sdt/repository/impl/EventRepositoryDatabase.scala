package com.sdt.repository.impl

import com.sdt.model.EventStatus.EventStatus
import com.sdt.model.EventType.EventType
import com.sdt.model._
import com.sdt.repository.EventRepository
import slick.dbio.Effect
import slick.jdbc.PostgresProfile.api._
import slick.jdbc.JdbcBackend
import slick.lifted.ProvenShape
import slick.sql.FixedSqlAction

import java.time.ZonedDateTime
import scala.concurrent.Future

class EventRepositoryDatabase(val db: JdbcBackend.Database) extends EventRepository {

  class EventsTable(tag: Tag) extends Table[Event](tag, "event") {

    def id: Rep[Long] = column("id", O.PrimaryKey, O.AutoInc)
    def eventType: Rep[EventType] = column("type_id")
    def status: Rep[EventStatus] = column("status_id")
    def plannedStartTime: Rep[ZonedDateTime] = column("planned_start_time")
    def plannedFinishTime: Rep[ZonedDateTime] = column("planned_finish_time")

    override def * : ProvenShape[Event] = (
      id.?,
      eventType,
      status,
      plannedStartTime,
      plannedFinishTime
      ).mapTo[Event]
  }

  private[impl] val allEvents = TableQuery[EventsTable]

  private val initSchema: FixedSqlAction[Unit, NoStream, Effect.Schema] = allEvents.schema.create

  def createTableForLocal: Future[Unit] = db.run(initSchema)

  def add(event: Event): Future[Event] =
    db.run(
      (allEvents returning allEvents.map(_.id)
        into ((event, eventId) => event.copy(id = Some(eventId)))
        ) += event
    )

  def getById(eventId: Long): Future[Option[Event]] =
    db.run(
      allEvents
        .filter(_.id === eventId)
        .result
        .headOption
    )

  def updateById(eventId: Long, event: Event): Future[Int] =
    db.run(
      allEvents
        .filter(_.id === eventId)
        .update(event)
    )

  def updateStatusById(eventId: Long, eventStatus: EventStatus.Val): Future[Int] =
    db.run(
      allEvents
        .filter(_.id === eventId)
        .map(_.status)
        .update(eventStatus)
    )
}
