package com.sdt.repository

import com.sdt.model._

import scala.concurrent.Future

trait EventRepository {
  def add(event: Event): Future[Event]
  def getById(eventId: Long): Future[Option[Event]]
  def updateById(eventId: Long, event: Event): Future[Int]
  def updateStatusById(eventId: Long, eventStatus: EventStatus.Val): Future[Int]
}
