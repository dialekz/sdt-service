package com.sdt.exception

sealed abstract class ServiceException(message: String) extends Exception(message)

final case class EventNotFoundException(eventId: Long)
  extends ServiceException(s"Event with id=$eventId is not found")
