package com.sdt.model

import com.sdt.model.EventStatus.EventStatus
import com.sdt.model.EventType.EventType

import java.time.ZonedDateTime

final case class Event(id: Option[Long],
                       eventType: EventType,
                       status: EventStatus,
                       plannedStartTime: ZonedDateTime,
                       plannedFinishTime: ZonedDateTime)
