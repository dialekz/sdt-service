package com.sdt.model

final case class ServiceInfo(id: Long,
                             name: String,
                             description: String)
