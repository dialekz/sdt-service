package com.sdt.model

import scala.language.implicitConversions

object EventStatus extends Enumeration {

  sealed case class Val(override val id: Int, code: Int) extends super.Val(id)

  implicit def valueToEventStatus(v: Value): Val = v.asInstanceOf[Val]

  type EventStatus = Val

  val EventCreated = Val(1, 100)
  val EventInProgress = Val(2, 200)
  val EventFinished = Val(3, 300)
  val EventRejected = Val(4, 310)

  def byId(statusId: Int): Val = values.find(_.id == statusId).get
  def byCode(statusCode: Int): Val = values.find(_.code == statusCode).get
}
