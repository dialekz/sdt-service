package com.sdt.model

import scala.language.implicitConversions

object EventType extends Enumeration {

  sealed case class Val(override val id: Int, name: String) extends super.Val(id)

  implicit def valueToEventType(v: Value): Val = v.asInstanceOf[Val]

  type EventType = Val

  val ServiceCrash = Val(1, "ServiceCrash")
  val PlannedWork = Val(2, "PlannedWork")

  def byId(typeId: Int): Val = values.find(_.id == typeId).get
  def byName(typeName: String): Val = values.find(_.name == typeName).get
}
