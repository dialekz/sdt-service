package com.sdt.api

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import com.sdt.model.EventStatus.EventStatus
import com.sdt.model.EventType.EventType
import com.sdt.model._
import spray.json._

import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter

trait ApiJsonSupport extends DefaultJsonProtocol with SprayJsonSupport {

  implicit val eventTypeFormat = new JsonFormat[EventType] {
    override def write(obj: EventType.Val): JsValue = JsString(obj.name)
    override def read(json: JsValue): EventType.Val = json match {
      case JsString(str) => EventType.byName(str)
      case x => throw new RuntimeException(s"Unexpected type ${x.getClass.getName} when trying to parse String")
    }
  }

  implicit val eventStatusFormat = new JsonFormat[EventStatus] {
    override def write(obj: EventStatus.Val): JsValue = JsNumber(obj.code)
    override def read(json: JsValue): EventStatus.Val = json match {
      case JsNumber(num) => EventStatus.byCode(num.intValue)
      case x => throw new RuntimeException(s"Unexpected type ${x.getClass.getName} when trying to parse Number")
    }
  }

  implicit val dateTimeFormat = new JsonFormat[ZonedDateTime] {
    override def write(obj: ZonedDateTime): JsValue = JsString(obj.format(DateTimeFormatter.ISO_ZONED_DATE_TIME))
    override def read(json: JsValue): ZonedDateTime = json match {
      case JsString(str) => ZonedDateTime.parse(str, DateTimeFormatter.ISO_ZONED_DATE_TIME)
      case x => throw new RuntimeException(s"Unexpected type ${x.getClass.getName} when trying to parse DateTime")
    }
  }

  implicit val eventFormat: RootJsonFormat[Event] = jsonFormat5(Event)
}
