package com.sdt.service

import com.sdt.model.Event

trait EventStatusSchedulerService {
  def createTaskToStartEvent(event: Event): Unit
  def createTaskToFinishEvent(event: Event): Unit
  def stopTasksForEvent(event: Event): Unit
}
