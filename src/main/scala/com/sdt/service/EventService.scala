package com.sdt.service

import com.sdt.model.Event

import scala.concurrent.Future

trait EventService {
  def getEventById(eventId: Long): Future[Event]
  def createEvent(event: Event): Future[Event]
  def updateEvent(eventId: Long, event: Event): Future[Event]
  def startEvent(eventId: Long): Future[Event]
  def finishEvent(eventId: Long): Future[Event]
  def rejectEvent(eventId: Long): Future[Event]
}
