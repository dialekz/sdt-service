package com.sdt.service.impl

import akka.actor.{ActorSystem, Cancellable}
import com.sdt.model._
import com.sdt.repository.EventRepository
import com.sdt.service.EventStatusSchedulerService
import com.typesafe.scalalogging.LazyLogging

import java.time.ZonedDateTime
import java.time.temporal.ChronoUnit
import scala.collection.concurrent.TrieMap
import scala.concurrent.ExecutionContext
import scala.concurrent.duration.{FiniteDuration, MILLISECONDS}

class EventStatusSchedulerServiceImpl(eventRepository: EventRepository)
                                     (implicit ac: ActorSystem, ec: ExecutionContext)
  extends EventStatusSchedulerService with LazyLogging {

  private val scheduler = ac.scheduler
  private[impl] val eventStatusTasks = new TrieMap[Long, Cancellable]

  override def createTaskToStartEvent(event: Event): Unit =
    createTaskForEvent(
      event.plannedStartTime,
      eventRepository.updateStatusById(event.id.get, EventStatus.EventInProgress)
        .andThen(_ => createTaskToFinishEvent(event))
    )(event)

  override def createTaskToFinishEvent(event: Event): Unit =
    createTaskForEvent(
      event.plannedFinishTime,
      eventRepository.updateStatusById(event.id.get, EventStatus.EventFinished)
    )(event)

  override def stopTasksForEvent(event: Event): Unit =
    eventStatusTasks.remove(event.id.get)
      .map(_.cancel)

  private def createTaskForEvent(startTime: ZonedDateTime, task: => Unit)(event: Event): Unit = startTime match {
    case time if time.isAfter(ZonedDateTime.now) =>
      val scheduledTask = scheduler.scheduleOnce(FiniteDuration(ZonedDateTime.now.until(time, ChronoUnit.MILLIS), MILLISECONDS))(task)
      eventStatusTasks.put(event.id.get, scheduledTask)
        .map(task => task.cancel())
    case _ => logger.warn(s"Can't create task for eventId=${event.id.get}, start time is before now")
  }
}
