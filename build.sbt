name := "sdt-service"
scalaVersion := "2.13.4"

Compile / scalaSource := baseDirectory.value / "src/main/scala"
Compile / resourceDirectory := baseDirectory.value / "src/main/resource"
Test / scalaSource := baseDirectory.value / "src/test/scala"

lazy val circeVersion = "0.13.0"
lazy val akkaVersion = "2.6.14"
lazy val akkaHttpVersion = "10.2.4"

ThisBuild / libraryDependencies ++= Seq(

  "com.typesafe.slick" %% "slick" % "3.3.3",
  "com.h2database" % "h2" % "1.4.200",
  "com.typesafe.slick" %% "slick-hikaricp" % "3.3.3",
  "org.postgresql" % "postgresql" % "9.4-1206-jdbc42",
  "com.typesafe" % "config" % "1.4.1",

  "com.typesafe.scala-logging" %% "scala-logging" % "3.9.2",
  "org.slf4j" % "slf4j-api" % "1.7.25",
  "ch.qos.logback" % "logback-classic" % "1.2.3",

  "de.heikoseeberger" %% "akka-http-circe" % "1.36.0",
  "com.typesafe.akka" %% "akka-actor" % akkaVersion,
  "com.typesafe.akka" %% "akka-stream" % akkaVersion,
  "com.typesafe.akka" %% "akka-http-spray-json" % akkaHttpVersion,

  "io.circe" %% "circe-core" % circeVersion,
  "io.circe" %% "circe-generic" % circeVersion,

  "org.scalatest" %% "scalatest" % "3.2.2" % Test,
  "org.scalamock" %% "scalamock" % "5.1.0" % Test,
  "com.typesafe.akka" %% "akka-http-testkit" % akkaHttpVersion % Test,
  "com.typesafe.akka" %% "akka-stream-testkit" % akkaVersion % Test
)